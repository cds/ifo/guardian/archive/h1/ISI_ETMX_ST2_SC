import time

from guardian import GuardState
from ezca.ligofilter import LIGOFilter, LIGOFilterManager

from .. import const as top_const
from . import const
from . import util

#############################################
# Make our own error class
class LIGOSensorCorrectionError(Exception):
    pass


##################################################
# Method 1
##################################################

# FIXME: Delete this? How about now? Now?
# General fm switching, given a dict of banks, dofs, fm#s
def gen_sc_filter_switching(chamber_config_dict, config, requestable=False):
    class sc_filter_switch_only(GuardState):
        """This is the basic state to switch Sensor Correction filters.
        
        Arguments:
        
        chamber_config_dict - The dictionary for the entire chamber with all of the
                              different configurations and their filters.
        config              - The letter of the config in teh dictionary to switch to
        requestable         - False by default. Make True if you'd like the state to 
                              be a requestable state.
        """
        request = requestable
        def main(self):
            # We should rewrite this to work how it really should
            # Probably get rid of the BS SCManager stuff

            # Find the banks, then go into the dof banks, then change the fms that are in that dof
            # Should we just use the config dictionary as the argument??? What other options do we have?
            for bank, dofs in chamber_config_dict[config].items():
                for dof, fms in dofs.items():
                    if util.is_switching_filters(bank, dof):
                        # FIXME: Need a better way to wait for it
                        # Wait for filters to finish switching
                        sleep_time = 15
                        time.sleep(sleep_time)
                        # If it's still ramping gain, raise an error
                        if util.is_switching_filters(sc_banks, dof_list):
                            raise LIGOSensorCorrectionError("%s SC is still ramping gains after {}sec" %(top_const.CHAMBER, sleep_time))
                    lf = LIGOFilter(const.SC_FILTER_CHANNEL_NAME.format(deg_of_free=dof, bank=bank), ezca)
                    # Ramp gain to 0
                    lf.ramp_gain(0, ramp_time=const.SC_RAMP_TIME, wait=False)
                    timer['gains down'] = const.SC_RAMP_TIME
                    timer['gains up'] = 0
                    
        def run(self):
            if timer['gains down'] and not timer['gains up']:
                # Change filters
                for bank, dofs in chamber_config_dict[config].items():
                    for dof, fms in dofs.items():
                        lf = LIGOFilter(const.SC_FILTER_CHANNEL_NAME.format(deg_of_free=dof, bank=bank), ezca)
                        # If it is still ramping somehow, then wait a few more seconds before erroring
                        if lf.is_gain_ramping():
                            time.sleep(3)
                            if lf.is_gain_ramping():
                                raise LIGOSensorCorrectionError('{bank} {dof} gain has been ramping too long.'.format(bank=bank, dof=dof))
                        # Turn off all but the ones in the deafult
                        all_fms = [x for x in range(1,11)]
                        fms_to_turn_off = []
                        for fm in all_fms:
                            # Grab the default filters from the default dictionary
                            if fm not in chamber_config_dict['DEFAULT']:
                                fms_to_turn_off.append(fm)
                        # Change filters
                        # We could have it wait to turn off all of the filters
                        lf.turn_off(fms_to_turn_off, engage_wait=True)
                        lf.turn_on(fms, engage_wait=True)
                        # Ramp gain back on when they are done
                        lf.ramp_gain(const.SC_FILTER_GAIN, ramp_time=const.SC_RAMP_TIME, wait=False)
                        # Set the last timer
                        timer['gains up'] = const.SC_RAMP_TIME
            if timer['gains down'] and timer['gains up']:
                return True

    return sc_filter_switch_only



# I was stupid, we dont actually need the above state
# The above will get too confusing and doesnt really stick to the clover
# pattern state graph that we have been using.
# KISS and just have small easy states:
# ENGAGING_ON_CONFIG_A, CONFIG_A, TURNING_OFF_CONFIG_A, CONFIG_DEFAULT, etc.
# This may make it easier to get auto-edges

def gen_toggle_config_filters_state(on_or_off, config_dict, index=False, requestable=False):
    """This state assumes that the default filters are already on.
    Then it will turn on the filters that it needs for that config
    as defined in the config dictionary that is fed to it.

    on_or_off   - Argument is either 'ON', or 'OFF' to turn on or off
                  the filters in the supplied config dictionary.
    config_dict - The dictionary for the desired configuration as 
                  defined in the configuration file. Ex: if you want
                  to turn on configuration B filters, feed in the
                  'B' dictionary.
    requestable - Make True if you would like the state to be requestable.
    """
    class TOGGLE_CONFIG_FILTERS(GuardState):
        def main(self):
            # Go through the different banks and dofs to turn the gains off
            for bank, dofs in config_dict.items():
                for dof, fms in dofs.items():
                    # Only do this if we are going to be turning something on
                    if fms:
                        lf = LIGOFilter(const.SC_FILTER_CHANNEL_NAME.format(deg_of_free=dof, bank=bank), ezca)
                        # Ramp gain to 0
                        lf.ramp_gain(0, ramp_time=const.SC_RAMP_TIME, wait=False)
            timer['gains ramping'] = const.SC_RAMP_TIME
            # This is just a marker so we can reuse the timer and not make a mess
            self.marker = 0
            
        def run(self):
            if timer['gains ramping'] and self.marker == 0:
                for bank, dofs in config_dict.items():
                    for dof, fms in dofs.items():
                        if fms:
                            # Turn on the new filters and do not wait
                            lf = LIGOFilter(const.SC_FILTER_CHANNEL_NAME.format(deg_of_free=dof, bank=bank), ezca)
                            # Do the ON or OFF thing
                            if on_or_off == 'ON':
                                # Maybe wait?
                                lf.turn_on(fms, engage_wait=False)
                            else:
                                lf.turn_off(fms, engage_wait=False)
                # FIXME: should we have an option where we leave the bank off?
                # This is in a separate loop so we can get through the fms faster
                for bank, dofs in config_dict.items():
                    for dof, fms in dofs.items():
                        if fms:
                            # Turn the gains back ON
                            lf = LIGOFilter(const.SC_FILTER_CHANNEL_NAME.format(deg_of_free=dof, bank=bank), ezca)
                            # Dont wait here, thats what the timer will be for
                            lf.ramp_gains(const.SC_FILTER_GAIN, ramp_time=const.SC_RAMP_TIME, wait=False)
                timer['gains ramping'] = const.SC_RAMP_TIME
                self.marker = 1
            elif timer['gains ramping'] and self.marker == 1:
                return True
    TOGGLE_CONFIG_FILTERS.request = requestable
    if index:
        TOGGLE_CONFIG_FILTERS.index = index
    return TOGGLE_CONFIG_FILTERS


# This could be a decorator as well
def check_config_engaged(config_dict):
    """Check that the filters are currently in their correct 
    configuration as defined by the config_dict passed in as
    an argument.

    Caution: This will only check if the fms in the dict are 
    engaged, it will not check if the other are disengaged.
    """
    for bank, dofs in config_dict.items():
        for dof, fms in dofs.items():
            if fms:
                # Check what should be engaged
                lf = LIGOFilter(const.SC_FILTER_CHANNEL_NAME.format(deg_of_free=dof, bank=bank), ezca)
                not_engaged = []
                for fm in fms:
                    if not lf.is_engaged('FM{}'.format(fm)):
                        not_engaged.append(fm)
                if not_engaged:
                    log('{} FMs:{} is not in the correct configuration'\
                        .format(const.SC_FILTER_CHANNEL_NAME\
                                .format(deg_of_free=dof, bank=bank), not_engaged))
                    notify('{} FMs:{} is not in the correct configuration'\
                           .format(const.SC_FILTER_CHANNEL_NAME\
                                   .format(deg_of_free=dof, bank=bank), not_engaged))
                    # Is this what we should do???
                    return False
    # If all is good,
    return True


##################################################
# Method 2
##################################################


########################################
# The below states are for a configuration that will only ramp between 
# two different SC filter bank gains. This assumes that the correct filters
# are already installed in the correct places and selected.
# This is the method used by LHO as of the end of O2.

# FIXME: nto sure if we still need this and if we do, we will need to change some thigns
def gen_sc_bank_gain_switch(old_bank, old_dof, new_bank, new_dof, ramp_time=const.DEFAULT_RAMP_TIME, requestable=False):
    class SC_BANK_GAIN_SWITCH(GuardState):
        """Act like a switch and turn the gain off in the old bank, and on in the new.

        """
        request = requestable
        def main(self):
            old_bank_chan = const.SC_FILTER_CHANNEL_NAME.format(deg_of_free=old_dof, bank=old_bank)
            new_bank_chan = const.SC_FILTER_CHANNEL_NAME.format(deg_of_free=new_dof, bank=new_bank)
            # Ramp OFF the gain of the old bank
            lf_old_bank = LIGOFilter(old_bank_chan, ezca)
            log('Ramping OFF the {} {} bank'.format(old_bank, old_dof))
            lf_old_bank.ramp_gain(0, ramp_time=ramp_time, wait=True)
            # Ramp ON the gain of the new bank
            lf_new_bank - LIGOFilter(new_bank_chan, ezca)
            log('Ramping ON the {} {} bank'.format(new_bank, new_dof))
            lf_new_bank.ramp_gain(1, ramp_time=ramp_time, wait=True)
            return True
    return SC_BANK_GAIN_SWITCH

# THis is the main one we use
def gen_sc_switch_one_bank(bank, dofs, gain, ramp_time=const.DEFAULT_RAMP_TIME, requestable=False):
    class SC_SWITCH_ONE_BANK(GuardState):
        """Ramp ON the gain of the bank and its dof(s) before the MATCH bank
        and then ramp ON the MATCH bank.
        """
        request = requestable
        def main(self):
            for dof in dofs:
                # Ramp ON the filter gain
                lf_bank = LIGOFilter(const.SC_FILTER_CHANNEL_NAME.format(deg_of_free=dof, bank=bank), ezca)
                # Changed to wait-False to try and speed things up
                lf_bank.ramp_gain(gain, ramp_time=ramp_time, wait=False)
                # Ramp ON the MATCH bank to allow the previous filter signal through
                lf_match = LIGOFilter(const.SC_FILTER_CHANNEL_NAME.format(deg_of_free=dof, bank='MATCH'), ezca)
                lf_match.ramp_gain(gain, ramp_time=ramp_time, wait=False)
            self.timer['ramping gains'] = ramp_time

        def run(self):
            if self.timer['ramping gains']:
                return True
            else:
                notify('Waiting for gains to ramp')
                return False
            
    return SC_SWITCH_ONE_BANK

def gen_sc_all_gains_off(bank_list, banks_dofs, ramp_time=const.DEFAULT_RAMP_TIME, requestable=False):
    class SC_ALL_GAINS_OFF(GuardState):
        """Turn OFF the gain in all of the banks listed in the degree of freedom
        specified. Ramp time can be set manually or as teh default.
        """
        request = requestable
        def main(self):
            for dof in banks_dofs:
                lfm = LIGOFilterManager([const.SC_FILTER_CHANNEL_NAME.format(deg_of_free=dof,bank=bank) for bank in bank_list], ezca)
                def gains_off(ligo_filter):
                        ligo_filter.ramp_gain(0, ramp_time=ramp_time, wait=False)
                lfm.all_do(gains_off)
            self.timer['ramping gains'] = ramp_time

        def run(self):
            if self.timer['ramping gains']:
                return True
            else:
                notify('Waiting for gains to ramp')
                return False

    return SC_ALL_GAINS_OFF


# This could be a decorator as well?
def check_config_path_gains(config_dict, engaged=True):
    """Check the gains for the paths that should be engaged
    currently. Configuration is defined by the config_dict 
    argument.

    If engaged=False, it will check if the gains are 0.

    Caution: This will only check if the gains in the dict are 
    engaged, it will not check if the other are disengaged.
    """
    if not engaged:
        expected_gain = 0
    else:
        expected_gain = 1
        
    for bank, dofs in config_dict.items():
        target_dofs = [dof for dof, fms in dofs.items() if fms]
        for dof in target_dofs:
            if ezca['{}_GAIN'.format(
                    const.SC_FILTER_CHANNEL_NAME.format(
                        deg_of_free=dof, bank=bank))] != expected_gain:
                # FIXME: This should notify, but for now we can just do this
                return False
    # If all is good,
    return True


#############################################
# State generators
#############################################

def get_idle_state(config_name, config_dict, index, requestable=True, engaged=True):
    """General idle state generator."""
    class IDLE_STATE(GuardState):
        request = requestable
        def main(self):
            # This should mainly be for rerequesting and getting the config correct.
            # Do the loop again and make sure that we have the correct gains
            if not check_config_path_gains(config_dict, engaged=engaged):
                log('Configuration not correct, turning off SC')
                return 'TURNING_OFF_CONFIG_{}'.format(config_name)
            else:
                return True
        def run(self):
            if not check_config_path_gains(config_dict, engaged=engaged):
                notify('Configuration has changed!')
            # Return True reguardless, don't want to be stuck in an odd config
            return True
    IDLE_STATE.index = index
    return IDLE_STATE


def make_config_idle_states(chamber_dict):
    """Autogenerate the idle states, defined by the configurations in 
    the sensor correction dictionary. This is designed to have all of
    the configuration states come from some DEFAULT state. The 
    transition states can also be autogenerated from the function 
    below, or made separately.

    The autogeneration of states is done by setting a state function
    equal to the state name in the global namespace. 

    This does not creat the DEFAULT state. This should be done by hand
    to customize for the site.
    """
    for i, config in enumerate(chamber_dict.items()):
        if config[0] != 'DEFAULT':
            globals()['CONFIG_{}'.format(config[0])] = get_idle_state(config[0], config[1], (i+1)*10)


# For method 1
def make_filter_transition_states(chamber_dict):
    """Set the state function to the global namespace state name to 
    autogen the appropriate transition states to and from the default
    state and the idle state. This does not include the idle states 
    for the case that a user may want to make their own transition 
    states, but autogen the idles.

    This is only for Method 1, and another function should be used
    to autogen the transition states for Method 2 (ramping only the
    gain between banks, not switching filters).
    """
    for i, config in enumerate(chamber_dict):
        if config != 'DEFAULT':
            globals()['ENGAGING_CONFIG_{}'.format(config)] =\
                    gen_toggle_config_filters(
                        'ON',
                        chamber_dict[config],
                        index=((i+1)*10)+2,
                        requestable=False)
            globals()['TURNING_OFF_CONFIG_{}'.format(config)] =\
                    gen_toggle_config_filters(
                        'OFF',
                        chamber_dict[config],
                        index=((i+1)*10)+7,
                        requestable=False)

# For method 2
def make_bank_transition_states(chamber_dict):
    """Set the state function to the global namespace state name to 
    autogen the appropriate transition states to and from the default
    state and the idle state. This does not include the idle states 
    for the case that a user may want to make their own transition 
    states, but autogen the idles.

    This is only for Method 2, and the above function should be used
    to autogen the transition states for Method 1 (switching between 
    filters and maybe banks as well).

    If you will be using the BRS SC then please label the name of the 
    configuration 'BRS' and then use the BRS_states.py to finish 
    making the correct states.
    """
    for i, config in enumerate(chamber_dict):
        if config != 'DEFAULT':
            if len(chamber_dict[config].keys()) > 1:
                raise LIGOSensorCorrectionError('Too many banks in configuration {}'.format(config))
            else:
                banklist = list(chamber_dict[config].keys())
            # Since these should only have one bank anyway
            bank = banklist[0]
            dofs = [dof for dof in chamber_dict[config][bank] if chamber_dict[config][bank][dof]]
            globals()['ENGAGING_CONFIG_{}'.format(config)] =\
                    gen_sc_switch_one_bank(
                        bank,
                        dofs,
                        const.SC_GAIN,
                        ramp_time=const.DEFAULT_RAMP_TIME,
                        requestable=False)
            globals()['TURNING_OFF_CONFIG_{}'.format(config)] =\
                    gen_sc_all_gains_off(
                        banklist,
                        dofs,
                        ramp_time=const.DEFAULT_RAMP_TIME,
                        requestable=False)

            
# Put it all together
def make_config_states(chamber_dict, method=1):
    """A Function that will call the above generator functions.
    method - Eiher 1 or 2 for the above described methods.
    """
    # Idles
    make_config_idle_states(chamber_dict)
    # Transitions
    if method == 1:
        make_filter_transition_states(chamber_dict)
    elif method == 2:
        make_bank_transition_states(chamber_dict)
    else:
        raise LIGOSensorCorrectionError('Method must be either 1 or 2')

